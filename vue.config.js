module.exports = {
  pwa: {
    name: 'Megalomyrmex Worker Taxonomic Key',
    workboxOptions: {
      skipWaiting: true,
      runtimeCaching: [{
        urlPattern: new RegExp('https://fonts.googleapis.com/.*'),
        method: 'GET',
        handler: 'cacheFirst',
        options: { cacheableResponse: { statuses: [0, 200] } }
      },
      {
        urlPattern: new RegExp('https://fonts.gstatic.com/.*'),
        method: 'GET',
        handler: 'cacheFirst',
        options: { cacheableResponse: { statuses: [0, 200] } }
      }]
    }
  },

  publicPath: '',
  outputDir: undefined,
  assetsDir: 'assets',
  runtimeCompiler: undefined,
  productionSourceMap: false,
  parallel: undefined,
  css: undefined
}