/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [].concat(self.__precacheManifest || [])
workbox.precaching.suppressWarnings()
workbox.precaching.precacheAndRoute(self.__precacheManifest, {})

// Listen to service worker updates and refresh the worker
self.addEventListener('message', msg => {
  if (msg.data.action === 'skipWaiting') {
    console.log('Updating service worker')
    self.skipWaiting()
  }
})
